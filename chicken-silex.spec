Name:		chicken-silex
Version:	1.4
Release:	1%{?dist}
Summary:	An efficient and powerful lexer generator

License:	BSD
URL:		http://wiki.call-cc.org/eggref/4/silex
Source0:	%{name}-%{version}.tar.gz

BuildRequires:	chicken
Requires:	chicken

%description
SILex is a lexical analyzer generator similar to the Lex and Flex programs, but
for Scheme. SILex stands for Scheme Implementation of Lex.

%prep
%autosetup -n %{name}-%{version}

%build
chicken-install -no-install -keep

%install
mkdir -p %{buildroot}/%{_includedir}/chicken/8/
cp silex.so %{buildroot}/%{_includedir}/chicken/8/silex.so
cp silex.import.so %{buildroot}/%{_includedir}/chicken/8/silex.import.so

%files
%{_includedir}/chicken/8/silex.so
%{_includedir}/chicken/8/silex.import.so

%changelog
* Sat Feb 03 2018 Timothée Floure <fnux@fedoraproject.org> - 1.4-1
- Let there be package.
