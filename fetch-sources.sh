#/bin/sh

EGG_NAME="silex"

if ! [ -x "$(command -v chicken-install)" ]; then
  echo 'Error: chicken-install is not installed.' >&2
  exit 1
fi

# Download source files
/usr/bin/chicken-install -r $EGG_NAME

# Build an archive from the source files
version=$(cat $EGG_NAME/$EGG_NAME.setup | egrep -o '([0-9]+.[0-9]+)')
mv $EGG_NAME chicken-$EGG_NAME-$version
/usr/bin/tar -cvzf chicken-$EGG_NAME-$version.tar.gz chicken-$EGG_NAME-$version

# Clean the downloaded file
rm -r chicken-$EGG_NAME-$version
